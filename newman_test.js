const newman = require("newman"); // require newman in your project

newman.run(
  {
    collection: require("./API TEST.postman_collection.json"),
    reporters: "cli",
  },
  function (err) {
    if (err) {
      throw err;
    }
    console.log("One");
    console.log("==================================");
    console.log("collection run complete!");
  }
);